Manual de instalacion

Para poder usar la landpage necesitamos:
    - Un servidor web que disponga
        - Php 7.0 o superior
        - Mysql server 5.7 o superior
        - Apache 2.4 o superior

Para insatalar la landpage lo haremos de la siguiente manera
    1º/ Instalar la base de datos que se encuentra en el directorio web/bbdd/bbdd.sql, para ello lo hacemos desde phpmyadmyn
        o desde la consola sql como el siguiente ejemplo mysql -u MyDatabaseUser -pMyPassword MyDatabase < C:\MySQLScript.sql
    2º/ Copiar todo el contenido de git al directorio donde deseamos ponerlo en el servidor
    3º/ Modificar el archivo config.yml que se ecuentra en la ruta "Configuration Files" para configurar el servidor de correo
        cambiando las siguientes lineas, por los valores que vayamos a usar, actualmente esta confgurado para poner usuario y
        contraseña de gmail
            swiftmailer:
            transport: smtp
            encryption: ssl
            auth_mode: login
            host: smtp.gmail.com
            username: ejemplo@gmail.com
            password: clave
            spool: { type: memory }
    4º/ Modificar el archivo parameters.yml, donde debemos de configurar el database_host, port, name, user y los parámetros
        para el correo, ejemplo para servidor local y gmail.
            parameters:
                database_host: 127.0.0.1
                database_port: null
                database_name: landpage
                database_user: root
                database_password: null
                mailer_transport: gmail
                mailer_host: ~
                mailer_user: ejemplo@gmail.com
                mailer_password: clave
                secret: ThisTokenIsNotSoSecretChangeIt
    5º/Cambiar el email de origen en el archivo clienteController.php en la ruta Source Files/app/src/AppBundle/Controller
        Cambiar las lineas 138 y 238 donde pondremos el mail de origen del correo

    6º/Configurar el servidor apache para que direccione a la ruta /web/app.php, para acceder pondremos url/promocion, 
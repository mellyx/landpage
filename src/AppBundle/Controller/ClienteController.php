<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Cliente;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Vehiculo;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use \Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Entity\Datospromocion;


/**
 * Cliente controller.
 *
 * 
 */
class ClienteController extends Controller
{
     /**
     * Crea una nueva entrada del cliente y genera el landpage
     *
     * @Route("/promocion", name="cliente_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $datos=new Datospromocion();
        //cogemos todos los elementos para crear el formulario de la base de datos
        
        $em = $this->getDoctrine()->getManager();
        $vehiculos = $em->getRepository('AppBundle:Vehiculo')->findAll();
        $preferencias = $em->getRepository('AppBundle:Preferencia')->findAll();
        $tipos = $em->getRepository('AppBundle:Tipo')->findAll();
        

        //los hacemos un array indexado por el nombre para que aparezca correctamente en el choice type
        $tiposchoice=[];
       
        foreach ($tipos as $tipo){
            $tiposchoice[$tipo->getTipo()]=$tipo;
        }
        $vehiculoschoice=[];//el de vehículos lo dejamos vacio para así rellenarlo más adelante
         foreach ($vehiculos as $vehiculo){
            $vehiculoschoice[$vehiculo->getNombre()]=$vehiculo;
        }
        $preferenciachoice=[];
        //si pasamos por parametro la preferencia horaria le ponemos esa por defecto
        //primero buscamos en la bbdd la preferencia pasada por get
        $preferenciaget = $em->getRepository('AppBundle:Preferencia')->findBynombre($request->get("preferencia"));
        if ($preferenciaget!=null)
        {
            $preferenciachoice[$preferenciaget[0]->getNombre()]=$preferenciaget[0];
            $datos->setPreferenciadellamada($preferenciaget[0]);
            $bloqueado=true;//esto es para bloquear el formulario
        }
        else 
        {//si el parametro no existe en la base de datos pues ponemos todos
            foreach ($preferencias as $preferencia){
                $preferenciachoice[$preferencia->getNombre()]=$preferencia;
            }
            $bloqueado=false;
        }
        //creamos el formulario
        $form =$this->createForm(\AppBundle\Form\DatospromocionType::class,$datos,array('tiposchoice'=>$tiposchoice,'vehiculoschoice'=>$vehiculoschoice,'preferenciachoice'=>$preferenciachoice,'bloqueado'=>$bloqueado));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            //recogemos los datos del formulario
            $datos=$form->getData();
            //si pasamos por get la preferencia se la asignamos
            if ($request->get("preferencia"))$datos->setPreferenciadellamada($preferenciaget[0]);
            //else $cliente->setPreferenciapreferencia($datos->getPreferenciadellamada());

            //Guardamos los datos en el cliente
            $cliente=new Cliente();
            $cliente->setNombre($datos->getNombre());
            $cliente->setApellidos($datos->getApellidos());
            $cliente->setEmail($datos->getEmail());
            $cliente->setTelefono($datos->getTelefono());
            $cliente->setPreferenciapreferencia($datos->getPreferenciadellamada());
            $cliente->setVehiculovehiculo( $datos->getVehiculo());
            
            //lo guardamos
            $em = $this->getDoctrine()->getManager();
            
            
            $validator = $this->get('validator');
            $errors = $validator->validate($cliente);
            
            if (count($errors) > 0) {
                return new Response(print_r($errors, true));
                
            } else {
                $em->persist($cliente);
                $em->flush();
                
                $session=new Session();
                $session->set("envio","enviook");

                //creamos el mensaje del correo
                $mensaje = \Swift_Message::newInstance()
                ->setSubject('Muchas gracias')
                ->setFrom('miempresa@ejemplo.com')//deberemos de poner el mail desde donde se envia
                ->setTo($datos->getEmail())
                ->setBody(
                $this->renderView(
                    'Cliente/email.html.twig',
                    array("nombre"=>$datos->getNombre(),"apellidos"=>$datos->getApellidos())
                    )
                )
                ;
                $this->get('mailer')->send($mensaje);
        
                
                 return $this->redirectToRoute('cliente_show', array('idcliente' => $cliente->getIdcliente()));
            }
            
            

            return $this->redirectToRoute('cliente_show', array('idcliente' => $cliente->getIdcliente()));
        }

        return $this->render('cliente/new.html.twig', array(
            'cliente' => $datos,
            'form' => $form->createView(),
        ));
    }

    
    /**
     * Funcion que devuelve los vehiculos según su categoria para ser usados en el javascript
     * @Route("/vehiculos", name="vehiculos")
     * @return Json objet
     */
    
    public function vehiculos(){
        $em = $this->getDoctrine()->getRepository(Vehiculo::class);
        //buscamos los vehículos todo terreno
        $query=$em->createQueryBuilder("vehiculo")->join('vehiculo.tipotipo','tipo')
            ->where('tipo.tipo=:nombre')
            ->setParameter(':nombre','Todo terreno')
            ->getQuery();
        $vehiculostt=$query->getResult();
        //ahora buscamos los vehiculos comerciales y turismo
        $query=$em->createQueryBuilder("vehiculo")->join('vehiculo.tipotipo','tipo')
            ->where('tipo.tipo=:nombre')
            ->setParameter(':nombre','Comercial')
            ->getQuery();
        $vehiculosnott=$query->getResult();
        
        //serializamos los objetos para poder ser utilizados desde el javascrip
        $vehiculos["todoterreno"]=$vehiculostt;
        $vehiculos["notodoterreno"]=$vehiculosnott;
        $encoders = array(new JsonEncoder());
        $normalizer = new ObjectNormalizer();
            $normalizer->setCircularReferenceLimit(1);
            $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
            });
            $normalizers = array($normalizer);
            $serializer = new Serializer($normalizers, $encoders);

            $normalizers = array($normalizer);
            $serializer = new Serializer($normalizers, $encoders);;
        $jProductos = $serializer->serialize($vehiculos, 'json');
        $response = new Response($jProductos);
        return $response;
        
    }
    
    /**
     * Funcion que guarda los valores de un cliente en la bbdd y envia un mail de confirmacion
     * @Route("/nuevoclientejs", name="nuevoclientejs")
     * @return Json objet
     */
    
    public function nuevoclientejs(Request $request)
    {
        //creamos las variables para controlar los errores
        $errnombre=true;
        $errapell=true;
        $erremail=true;
        $errtlfn=true;
        $errvehi=true;
        $errprefe=true;
        //creamos un cliente y le asignamos los valores para introducirlo en la BBDD
        $cliente = new Cliente();
        //comprobamos que todos los datos son correctos antes de insertarlos en la bbdd
        $datos=$request->get("datos");
        if (!empty($datos["nombre"]) && strlen($datos["nombre"])<=45)$cliente->setNombre($datos["nombre"]);
        else $errnombre=false;
        if (!empty($datos["apellidos"])&& strlen($datos["apellidos"])<=150)$cliente->setApellidos($datos["apellidos"]);
        else $errapell=false;
        if ((!empty($datos["email"]))&&( false !== filter_var($datos["email"], FILTER_VALIDATE_EMAIL))&& (strlen($datos["email"])<=150))$cliente->setEmail($datos["email"]);
        else $erremail=false;
        if (strlen($datos["tlfn"])==9 && preg_match("/^[9|6|7][0-9]{8}$/",$datos["tlfn"]))$cliente->setTelefono($datos["tlfn"]);
        else $errtlfn=false;
        $em = $this->getDoctrine()->getManager();
        //tenemos que buscar el objeto de tipo vehículo y preferencia para poder asignarlo
        $vehiculo = $em->getRepository('AppBundle:Vehiculo')->findByNombre($datos["vehiculo"]);
        if ($vehiculo==null) $errvehi=false;
        else $cliente->setVehiculovehiculo( $vehiculo[0]);
        
        $preferencia = $em->getRepository('AppBundle:Preferencia')->findByNombre($datos["preferencia"]);
        if ($preferencia==null) $errprefe=false;
        else $cliente->setPreferenciapreferencia($preferencia[0]);
        
        //guardamos el cliente en la bbdd si no hay errores
        if ($errapell && $erremail && $errnombre && $errprefe && $errtlfn && $errvehi){
            $em->persist($cliente);
            $em->flush();
        }else{
            //si no son correctos los datos mostraremos error en la pantalla
            //esto solo pasa si alguien toca donde no debe antes de pulsar enviar
            //así queda mas protegido el servidor
            return "Error";
        }
        
        //ahora creamos la variable de sesión para que podamos abrir la ventana de gracias-promocion
        $session=new Session();
        $session->set("envio","enviook");
        
        //creamos el mensaje del correo
        $mensaje = \Swift_Message::newInstance()
        ->setSubject('Muchas gracias')
        ->setFrom('miempresa@ejemplo.com')//deberemos de poner el mail desde donde se envia
        ->setTo($datos["email"])
        ->setBody(
        $this->renderView(
            'Cliente/email.html.twig',
            array("nombre"=>$datos["nombre"],"apellidos"=>$datos["apellidos"])
            )
        )
        ;
        $this->get('mailer')->send($mensaje);
        
                
        //creamos la respuesta de ok
        $encoders = array(new JsonEncoder());
        $normalizer = new ObjectNormalizer();
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $normalizers = array($normalizer);
        $serializer = new Serializer($normalizers, $encoders);
        $jProductos = $serializer->serialize("ok", 'json');
        $response = new Response($jProductos);
        return $response;
        
    }
    
    /**
     * Método que gerera la web de gracias
     * @Route("/promocion/gracias-promocion", name="gracias")
     * @return redireccion si error
     */
    
    public function graciaspromocion(Request $request)
    {
        $session=$request->getSession();
        if($session->get("envio")=="enviook")
        {
            $session->remove("envio");
             return $this->render('cliente/gracias.html.twig');
            
        }else{
           //redirigimos
           return $this->redirectToRoute('cliente_new');
        }
    }
    
}

<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use \Symfony\Component\Form\Extension\Core\Type\SubmitType;


class DatospromocionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //$builder->add('nombre')->add('apellidos')->add('telefono')->add('email')->add('tipo_vehiculo')->add('preferencia')->add('vehiculo');
        $builder->add("Nombre",textType::class,
                        array('label' => 'Nombre(*)','required' => true, 'label_attr'=> array('class'=>'col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4'),'attr'=>array('class'=>'col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8')))
                ->add("Apellidos",textType::class,
                        array('label' => 'Apellidos(*)','required' => true, 'label_attr'=> array('class'=>'col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4'),'attr'=>array('class'=>'col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8')))
                ->add("Telefono",textType::class,
                        array('label' => 'Teléfono','required' => false, 'label_attr'=> array('class'=>'col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4'),'attr'=>array('class'=>'col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8')))
                ->add("Email",textType::class,
                        array('label' => 'Email(*)','required' => true, 'label_attr'=> array('class'=>'col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4'),'attr'=>array('class'=>'col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8')))
                ->add("Tipo_de_Vehiculo",choiceType::class,
                        array('choices'=>$options['tiposchoice'],'label' => 'Tipo de vehículo(*)','required' => true, 'label_attr'=> array('class'=>'col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4'),'attr'=>array('class'=>'col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8')))
                ->add("Vehiculo",choiceType::class,
                        array('choices'=>$options['vehiculoschoice'],'label' => 'Vehículo(*)','required' => true, 'label_attr'=> array('class'=>'col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4'),'attr'=>array('class'=>'col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8')))
                ->add("Preferencia_de_llamada",choiceType::class,
                        array('choices'=>$options['preferenciachoice'],
                            'disabled'=>$options['bloqueado'],//bloqueamos o no según se haya creado el formulario con o sin parámetros
                            'label' => 'Preferencia de llamada(*)','required' => true, 'label_attr'=> array('class'=>'col-12 col-sm-4 col-md-12 col-lg-4 col-xl-4'),'attr'=>array('class'=>'col-12 col-sm-8 col-md-12 col-lg-8 col-xl-8')));
                
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Datospromocion','tiposchoice'=>null,'vehiculoschoice'=>null,'preferenciachoice'=>null,'bloqueado'=>null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_cliente';
    }


}

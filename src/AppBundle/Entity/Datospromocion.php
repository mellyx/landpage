<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Datospromocion
{
    /**
     * @var string
     * @Assert\NotBlank(
     *      message = "El campo Nombre no puede estar vacio"
     * )
     * @Assert\Length(
     *      min = 1,
     *      max = 45,
     *      minMessage = "El nombre tiene que tener al menos {{ limit }} letras",
     *      maxMessage = "El nombre no puede tener mas de {{ limit }} letras"
     * )
     *
     */
    private $nombre;

    /**
     * @var string
     *  
     * @Assert\NotBlank(
     *      message = "El campo Apellidos no puede estar vacio"
     * )
     * @Assert\Length(
     *      min = 1,
     *      max = 150,
     *      minMessage = "El apellido tiene que tener al menos {{ limit }} letras",
     *      maxMessage = "El apellido no puede tener mas de {{ limit }} letras"
     * )
     *
     * 
     */
    private $apellidos;

    /**
     * @var string
     *
     * @Assert\Length(
     *      min = 0,
     *      max = 9,
     *      minMessage = "El telefono tiene que tener al menos {{ limit }} numeros",
     *      maxMessage = "El telefono no puede tener mas de {{ limit }} numeros"
     * )
     * 
     * @Assert\Regex(
     *     pattern="/^[9|6|7][0-9]{8}$/",
     *     match=true,
     *     message="el telefono '{{ value }}' no es válido"
     * )
     * 
     */
    private $telefono;

    /**
     * @var string
     * @Assert\NotBlank(
     *      message = "El campo email no puede estar vacio"
     * )
     * 
     * @Assert\Length(
     *      min = 0,
     *      max = 150,
     *      minMessage = "El email tiene que tener al menos {{ limit }} caracteres",
     *      maxMessage = "El email no puede tener mas de {{ limit }} caracteres"
     * )
     * @Assert\Email(
     *     message = "El email '{{ value }}' no es valido.",
     *     checkMX = true
     * )
     * 
     */
    private $email;
    
    /**
     * @var Vehiculo
     * @Assert\NotBlank(
     *      message = "El campo tipo de vehiculo no puede estar vacio"
     * )
     * 
     */
    
    private $tipo_de_vehiculo;
    /**
     * @var Vehiculo
     * @Assert\NotBlank(
     *      message = "El campo vehiculo no puede estar vacio"
     * )
     * 
     */
    private $vehiculo;
    
    /**
     * @var Preferencia
     * @Assert\NotBlank(
     *      message = "El campo preferencia no puede estar vacio"
     * )
     */
    private $preferencia_de_llamada;
    
    function getNombre() {
        return $this->nombre;
    }

    function getApellidos() {
        return $this->apellidos;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function getEmail() {
        return $this->email;
    }

    function getVehiculo(){
        return $this->vehiculo;
    }

    function getPreferenciadellamada(){
        return $this->preferencia_de_llamada;
    }
    function getTipodevehiculo(){
        return $this->tipo_de_vehiculo;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setApellidos($apellidos) {
        $this->apellidos = $apellidos;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setVehiculo($vehiculo) {
        $this->vehiculo = $vehiculo;
    }

    function setPreferenciadellamada($preferencia_de_llamada) {
        $this->preferencia_de_llamada = $preferencia_de_llamada;
    }
    function setTipodevehiculo($tipo_de_vehiculo) {
        $this->tipo_de_vehiculo = $tipo_de_vehiculo;
    }




}
<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tipo
 *
 * @ORM\Table(name="tipo")
 * @ORM\Entity
 */
class Tipo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idTipo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idtipo;

    /**
     * @var string
     *
     * @ORM\Column(name="Tipo", type="string", length=45, nullable=false)
     */
    private $tipo;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Vehiculo", mappedBy="tipotipo")
     */
    private $vehiculovehiculo;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->vehiculovehiculo = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get idtipo
     *
     * @return integer
     */
    public function getId()
    {
        return $this->idtipo;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Add vehiculovehiculo
     *
     * @param \AppBundle\Entity\Vehiculo $vehiculovehiculo
     *
     * @return Tipo
     */
    public function addVehiculovehiculo(\AppBundle\Entity\Vehiculo $vehiculovehiculo)
    {
        $this->vehiculovehiculo[] = $vehiculovehiculo;

        return $this;
    }

    /**
     * Remove vehiculovehiculo
     *
     * @param \AppBundle\Entity\Vehiculo $vehiculovehiculo
     */
    public function removeVehiculovehiculo(\AppBundle\Entity\Vehiculo $vehiculovehiculo)
    {
        $this->vehiculovehiculo->removeElement($vehiculovehiculo);
    }

    /**
     * Get vehiculovehiculo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVehiculovehiculo()
    {
        return $this->vehiculovehiculo;
    }
    /**
     * Convierte a string una preferencia
     */
    public function __toString() {
        return $this->tipo;
    }
}

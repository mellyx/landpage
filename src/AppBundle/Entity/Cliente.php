<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cliente
 *
 * @ORM\Table(name="cliente", indexes={@ORM\Index(name="fk_Cliente_Vehiculo1_idx", columns={"Vehiculo_idVehiculo"}), @ORM\Index(name="fk_Cliente_Preferencia1_idx", columns={"Preferencia_idPreferencia"})})
 * @ORM\Entity
 */
class Cliente
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idCliente", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcliente;

    /**
     * @var string
     * @ORM\Column(name="Nombre", type="string", length=45, nullable=false)
     * @Assert\NotBlank(
     *      message = "El campo no puede estar vacio"
     * )
     * @Assert\Length(
     *      min = 1,
     *      max = 45,
     *      minMessage = "El nombre tiene que tener al menos {{ limit }} letras",
     *      maxMessage = "El nombre no puede tener mas de {{ limit }} letras"
     * )
     *
     */
    private $nombre;

    /**
     * @var string
     * 
     * @ORM\Column(name="Apellidos", type="string", length=150, nullable=false)
     * 
     * @Assert\NotBlank(
     *      message = "El campo no puede estar vacio"
     * )
     * @Assert\Length(
     *      min = 1,
     *      max = 150,
     *      minMessage = "El apellido tiene que tener al menos {{ limit }} letras",
     *      maxMessage = "El apellido no puede tener mas de {{ limit }} letras"
     * )
     *
     * 
     */
    private $apellidos;

    /**
     * @var string
     *
     * @ORM\Column(name="Telefono", type="string", length=9, nullable=true)
     * @Assert\Length(
     *      min = 0,
     *      max = 9,
     *      minMessage = "El telefono tiene que tener al menos {{ limit }} numeros",
     *      maxMessage = "El telefono no puede tener mas de {{ limit }} numeros"
     * )
     * 
     * @Assert\Regex(
     *     pattern="/^[9|6|7][0-9]{8}$/",
     *     match=true,
     *     message="el telefono '{{ value }}' no es válido"
     * )
     * 
     */
    private $telefono;

    /**
     * @var string
     * @ORM\Column(name="Email", type="string", length=150, nullable=false)
     * @Assert\NotBlank(
     *      message = "El campo email no puede estar vacio"
     * )
     * 
     * @Assert\Length(
     *      min = 0,
     *      max = 150,
     *      minMessage = "El email tiene que tener al menos {{ limit }} caracteres",
     *      maxMessage = "El email no puede tener mas de {{ limit }} caracteres"
     * )
     * @Assert\Email(
     *     message = "El email '{{ value }}' no es valido.",
     *     checkMX = true
     * )
     * 
     */
    private $email;

    /**
     * @var \Preferencia
     *
     * @ORM\ManyToOne(targetEntity="Preferencia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Preferencia_idPreferencia", referencedColumnName="idPreferencia")
     * })
     */
    private $preferenciapreferencia;

    /**
     * @var \Vehiculo
     *
     * @ORM\ManyToOne(targetEntity="Vehiculo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Vehiculo_idVehiculo", referencedColumnName="idVehiculo")
     * })
     */
    private $vehiculovehiculo;



    /**
     * Get idcliente
     *
     * @return integer
     */
    public function getIdcliente()
    {
        return $this->idcliente;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Cliente
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     *
     * @return Cliente
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return Cliente
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Cliente
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set preferenciapreferencia
     *
     * @param \AppBundle\Entity\Preferencia $preferenciapreferencia
     *
     * @return Cliente
     */
    public function setPreferenciapreferencia(\AppBundle\Entity\Preferencia $preferenciapreferencia = null)
    {
        $this->preferenciapreferencia = $preferenciapreferencia;

        return $this;
    }

    /**
     * Get preferenciapreferencia
     *
     * @return \AppBundle\Entity\Preferencia
     */
    public function getPreferenciapreferencia()
    {
        return $this->preferenciapreferencia;
    }

    /**
     * Set vehiculovehiculo
     *
     * @param \AppBundle\Entity\Vehiculo $vehiculovehiculo
     *
     * @return Cliente
     */
    public function setVehiculovehiculo(\AppBundle\Entity\Vehiculo $vehiculovehiculo = null)
    {
        $this->vehiculovehiculo = $vehiculovehiculo;

        return $this;
    }

    /**
     * Get vehiculovehiculo
     *
     * @return \AppBundle\Entity\Vehiculo
     */
    public function getVehiculovehiculo()
    {
        return $this->vehiculovehiculo;
    }
}

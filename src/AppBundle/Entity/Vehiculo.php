<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vehiculo
 *
 * @ORM\Table(name="vehiculo")
 * @ORM\Entity
 */
class Vehiculo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idVehiculo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idvehiculo;

    /**
     * @var string
     *
     * @ORM\Column(name="Nombre", type="string", length=45, nullable=false)
     */
    private $nombre;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Tipo", inversedBy="vehiculovehiculo")
     * @ORM\JoinTable(name="vehiculo_has_tipo",
     *   joinColumns={
     *     @ORM\JoinColumn(name="Vehiculo_idVehiculo", referencedColumnName="idVehiculo")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="Tipo_idTipo", referencedColumnName="idTipo")
     *   }
     * )
     */
    private $tipotipo;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tipotipo = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get idvehiculo
     *
     * @return integer
     */
    public function getId()
    {
        return $this->idvehiculo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Vehiculo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add tipotipo
     *
     * @param \AppBundle\Entity\Tipo $tipotipo
     *
     * @return Vehiculo
     */
    public function addTipotipo(\AppBundle\Entity\Tipo $tipotipo)
    {
        $this->tipotipo[] = $tipotipo;

        return $this;
    }

    /**
     * Remove tipotipo
     *
     * @param \AppBundle\Entity\Tipo $tipotipo
     */
    public function removeTipotipo(\AppBundle\Entity\Tipo $tipotipo)
    {
        $this->tipotipo->removeElement($tipotipo);
    }

    /**
     * Get tipotipo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTipotipo()
    {
        return $this->tipotipo;
    }
    
    /**
     * Convierte a string una preferencia
     */
    public function __toString() {
        return $this->nombre;
    }
}

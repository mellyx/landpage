<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Preferencia
 *
 * @ORM\Table(name="preferencia")
 * @ORM\Entity
 */
class Preferencia
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idPreferencia", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idpreferencia;

    /**
     * @var string
     *
     * @ORM\Column(name="Nombre", type="string", length=45, nullable=false)
     */
    private $nombre;



    /**
     * Get idpreferencia
     *
     * @return integer
     */
    public function getIdpreferencia()
    {
        return $this->idpreferencia;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Preferencia
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    
    /**
     * Convierte a string una preferencia
     */
    public function __toString() {
        return $this->nombre;
    }
}

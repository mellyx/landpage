/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(window).ready(function(){
    var vehiculostt;
    var vehiculosnott;  
    var opcionesnott="";
    var opcionestt="";
    //animamos el logo
    $("#logo").animate({right: '+=5%'}, "slow");
    $("#logo").animate({top: '+=2%'}, "slow");
    $("#ocultar").show();//mostramos el icono de carga
    $("#enviar").prop("disabled",true);//bloqueamos el boton de enviar
    //hacemos una peticion de los vehículos que hay en la bbdd y generamos los option
    $.ajax({
                
        type: "GET",
        url: "./vehiculos",
        dataType: "json",
        data: {},  
      
        success : function(response) 
        {
           $("#ocultar").hide();
           $("#enviar").prop("disabled",false);//desbloqueamos el boton de enviar

           vehiculostt=response["todoterreno"];//guardamos los vehículos tt
           vehiculosnott=response["notodoterreno"];//guadamos los vehiculos comerciales y turismos
           
                    
            //se crean las opciones para el select
            //ponemos -1 al id para corregir el orden
            
            for (var i=0;i<vehiculosnott.length;i++) {
                var id=vehiculosnott[i]["id"]-1;
                opcionesnott+="<option value='"+id+"'>"+vehiculosnott[i]["nombre"]+"</option>";

            }
            //ponemos -1 al id para corregir el orden
            for (var i=0;i<vehiculostt.length;i++) {
                var id=vehiculosnott[i]["id"]-1;
                opcionestt+="<option value='"+id+"'>"+vehiculostt[i]["nombre"]+"</option>";

            }
            //siempre cargamos los vehículos nott ya que por defecto arranca el programa con turismo

            $("#appbundle_cliente_Vehiculo").html(opcionesnott);
             
        },
         error: function(){
            $("#ocultar").hide();
                alert("Ha habido un problema, no se ha podido procesar contacte con el administrador");
        }
    });
    
    
    
    //cuando cambiemos entre un tipo de vehículo u otro que cambie los valores del los vehículos
    $("#appbundle_cliente_Tipo_de_Vehiculo").change(function(){
        var seleccionado=$("#appbundle_cliente_Tipo_de_Vehiculo option:selected").text();
        if(seleccionado=="Todo terreno"){
           
            $("#appbundle_cliente_Vehiculo").html(opcionestt);
        }else{
         
            $("#appbundle_cliente_Vehiculo").html(opcionesnott);
        }
        
    });
     
    //vamos a validar en cliente
    
    $("#enviar").click(function(ev){
        //variables para comprobar que todo esta correcto
        var ncorrecto=true;//nombre
        var apcorrecto=true;//apellidos
        var ecorrecto=true;//email
        var tcorrecto=true;//telefono        
        
        ev.preventDefault();
        
        var nombre=$("#appbundle_cliente_Nombre").val();
        var apellidos=$("#appbundle_cliente_Apellidos").val();
        var tlfn=$("#appbundle_cliente_Telefono").val();
        var email=$("#appbundle_cliente_Email").val();
        var tvehiculo=$("#appbundle_cliente_Tipo_de_Vehiculo option:selected").text();
        var vehiculo=$("#appbundle_cliente_Vehiculo option:selected").text();
        var prefenrecia=$("#appbundle_cliente_Preferencia_de_llamada option:selected").text();
        
        //validamos el nombre
        if(nombre==""){
            ncorrecto=false;
            $("#appbundle_cliente_Nombre").attr("placeholder","Campo obligatorio");
            $("#appbundle_cliente_Nombre").css("borderColor","red");
            
        }else{
            ncorrecto=true;
            $("#appbundle_cliente_Nombre").css("borderColor","initial");
            
        }
        //validamos los apellidos
        if(apellidos==""){
            apcorrecto=false;
            $("#appbundle_cliente_Apellidos").attr("placeholder","Campo obligatorio");
            $("#appbundle_cliente_Apellidos").css("borderColor","red");
            
        }else{
            ncorrecto=true;
            $("#appbundle_cliente_Apellidos").css("borderColor","initial");
            
        }
        
        //validamos el telefono
        regextlfn= /^[9|6|7][0-9]{8}$/;//expresion regular del telefono
        if(tlfn!="" && !regextlfn.test($("#appbundle_cliente_Telefono").val().trim())){
            tcorrecto=false;
            //$("#errortlfn2").show();
            $("#appbundle_cliente_Telefono").css("borderColor","red");
            $("#appbundle_cliente_Telefono").val("");
            $("#appbundle_cliente_Telefono").attr("placeholder","Formato incorrecto:Introuzca ej.910000000");
            
        } else{
            tcorrecto=true;
            $("#appbundle_cliente_Telefono").css("borderColor","initial");
        }
        
        //validamos el email
        regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;//expresion regular email
        if (email!="" && !regex.test($('#appbundle_cliente_Email').val().trim())) {
            ecorrecto=false;
             $("#appbundle_cliente_Email").val("");
             $("#appbundle_cliente_Email").attr("placeholder","Formato incorrecto");
             $("#appbundle_cliente_Email").css("borderColor","red");
        }else{
            ecorrecto=true;
            
            if (email!="")$("#appbundle_cliente_Email").css("borderColor","initial");
        }
        
        if(email==""){
           ecorrecto=false;
           $("#appbundle_cliente_Email").val("");
           $("#appbundle_cliente_Email").attr("placeholder","Campo obligatorio");
           $("#appbundle_cliente_Email").css("borderColor","red"); 
        }
        
        
        
        //si estan todos los datos correctos
        //hacemos el servivio para mandar el mail y guardar en la base de datos
        if(ecorrecto && ncorrecto && apcorrecto && tcorrecto){
            $("#ocultar").show();
            $("#enviar").hide();
            var datos={
                "nombre":nombre,
                "apellidos":apellidos,
                "tlfn":tlfn,
                "email":email,
                "tvehiculo":tvehiculo,
                "vehiculo":vehiculo,
                "preferencia":prefenrecia
            };
            $.ajax({

                type: "POST",
                url: "./nuevoclientejs",
                dataType: "json",
                data: {datos},  
                success : function(response) 
                {
                   $("#ocultar").hide();
                   window.location.href=window.location.href+"/gracias-promocion";

                },
                 error: function(){
                    $("#ocultar").hide();
                    $("#enviar").show();
                        alert("Ha habido un problema, no se ha podido procesar contacte con el administrador");
                }
            });
        }else{
            $("#ocultar").hide();
        }
        
       
    });
});


-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 30-06-2018 a las 16:39:50
-- Versión del servidor: 5.7.19
-- Versión de PHP: 7.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `landpage`
--
CREATE DATABASE IF NOT EXISTS `landpage` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `landpage`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

DROP TABLE IF EXISTS `cliente`;
CREATE TABLE IF NOT EXISTS `cliente` (
  `idCliente` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `Apellidos` varchar(150) NOT NULL,
  `Telefono` varchar(9) DEFAULT NULL,
  `Email` varchar(150) NOT NULL,
  `Vehiculo_idVehiculo` int(11) NOT NULL,
  `Preferencia_idPreferencia` int(11) NOT NULL,
  PRIMARY KEY (`idCliente`),
  KEY `fk_Cliente_Vehiculo1_idx` (`Vehiculo_idVehiculo`),
  KEY `fk_Cliente_Preferencia1_idx` (`Preferencia_idPreferencia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preferencia`
--

DROP TABLE IF EXISTS `preferencia`;
CREATE TABLE IF NOT EXISTS `preferencia` (
  `idPreferencia` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`idPreferencia`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `preferencia`
--

INSERT INTO `preferencia` (`idPreferencia`, `Nombre`) VALUES
(1, 'Mañana'),
(2, 'Tarde'),
(3, 'Noche');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo`
--

DROP TABLE IF EXISTS `tipo`;
CREATE TABLE IF NOT EXISTS `tipo` (
  `idTipo` int(11) NOT NULL AUTO_INCREMENT,
  `Tipo` varchar(45) NOT NULL,
  PRIMARY KEY (`idTipo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo`
--

INSERT INTO `tipo` (`idTipo`, `Tipo`) VALUES
(1, 'Turismo'),
(2, 'Comercial'),
(3, 'Todo terreno');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculo`
--

DROP TABLE IF EXISTS `vehiculo`;
CREATE TABLE IF NOT EXISTS `vehiculo` (
  `idVehiculo` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`idVehiculo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `vehiculo`
--

INSERT INTO `vehiculo` (`idVehiculo`, `Nombre`) VALUES
(1, 'Corsa'),
(2, 'Astra'),
(3, 'Mokka'),
(4, 'Crossland');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculo_has_tipo`
--

DROP TABLE IF EXISTS `vehiculo_has_tipo`;
CREATE TABLE IF NOT EXISTS `vehiculo_has_tipo` (
  `Vehiculo_idVehiculo` int(11) NOT NULL,
  `Tipo_idTipo` int(11) NOT NULL,
  PRIMARY KEY (`Vehiculo_idVehiculo`,`Tipo_idTipo`),
  KEY `fk_Vehiculo_has_Tipo_Tipo1_idx` (`Tipo_idTipo`),
  KEY `fk_Vehiculo_has_Tipo_Vehiculo1_idx` (`Vehiculo_idVehiculo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `vehiculo_has_tipo`
--

INSERT INTO `vehiculo_has_tipo` (`Vehiculo_idVehiculo`, `Tipo_idTipo`) VALUES
(1, 1),
(2, 1),
(1, 2),
(2, 2),
(3, 3),
(4, 3);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `fk_Cliente_Preferencia1` FOREIGN KEY (`Preferencia_idPreferencia`) REFERENCES `preferencia` (`idPreferencia`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Cliente_Vehiculo1` FOREIGN KEY (`Vehiculo_idVehiculo`) REFERENCES `vehiculo` (`idVehiculo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `vehiculo_has_tipo`
--
ALTER TABLE `vehiculo_has_tipo`
  ADD CONSTRAINT `fk_Vehiculo_has_Tipo_Tipo1` FOREIGN KEY (`Tipo_idTipo`) REFERENCES `tipo` (`idTipo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Vehiculo_has_Tipo_Vehiculo1` FOREIGN KEY (`Vehiculo_idVehiculo`) REFERENCES `vehiculo` (`idVehiculo`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
